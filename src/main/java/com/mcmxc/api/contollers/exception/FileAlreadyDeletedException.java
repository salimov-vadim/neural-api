package com.mcmxc.api.contollers.exception;

import java.io.IOException;

public class FileAlreadyDeletedException extends IOException {

    public FileAlreadyDeletedException(String message) {
        super(message);
    }
}
