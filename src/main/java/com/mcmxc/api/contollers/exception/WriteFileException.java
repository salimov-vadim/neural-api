package com.mcmxc.api.contollers.exception;

import java.io.IOException;

public class WriteFileException extends IOException {
    public WriteFileException() {
    }

    public WriteFileException(String message) {
        super(message);
    }

    public WriteFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public WriteFileException(Throwable cause) {
        super(cause);
    }
}
