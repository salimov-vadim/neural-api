package com.mcmxc.api.contollers;

import com.mcmxc.api.contollers.exception.FileAlreadyDeletedException;
import com.mcmxc.api.dto.ResultDto;
import com.mcmxc.api.dto.ResultId;
import com.mcmxc.api.repository.TestJobRepository;
import com.mcmxc.api.util.FilesUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/neural-images")
public class FileUploadAndGetResultController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(FileUploadAndGetResultController.class);


    @Autowired
    private JobLauncher jobLauncher;


    @Autowired
    @Qualifier(value = "pythonJob")
    Job pythonJob;

    @Autowired
    @Qualifier(value = "checkIsResultReady")
    Job checkResultJob;

    @Autowired
    private TestJobRepository testJobRepository;

    @Autowired
    private FilesUtil filesUtil;


    @ApiOperation(value = "return id of job, send contentImage file and other parameters")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResultId uploadFile(@RequestParam("firstImage") MultipartFile firstImage,
                               @RequestParam("secondImage") MultipartFile secondImage,
                               @RequestParam(value = "weight", required = false) BigDecimal weight,
                               @RequestParam(value = "styleWeight", required = false) BigDecimal styleWeight,
                               @RequestParam(value = "scale", required = false) Float scale,
                               @RequestParam(value = "original", required = false) Boolean original
    ) throws Exception {

        if (firstImage.getOriginalFilename().equalsIgnoreCase(secondImage.getOriginalFilename())) {

        }

        File inputDir = filesUtil.createTempDirectory("neural");

        Map<String, JobParameter> confMap = new HashMap<String, JobParameter>();

        File firstImageFile = filesUtil.writeInputFile(firstImage, inputDir);
        File secondImageFile = filesUtil.writeInputFile(secondImage, inputDir);

        writeProperties(confMap, inputDir, "inputDir");
        writeProperties(confMap, firstImageFile, "firstImage");
        writeProperties(confMap, secondImageFile, "secondImage");
        writeProperties(confMap, weight, "weight");
        writeProperties(confMap, styleWeight, "styleWeight");
        writeProperties(confMap, scale, "scale");
        writeProperties(confMap, original, "original");


        JobParameters jobParameters = new JobParameters(confMap);
        jobLauncher.run(pythonJob, jobParameters);

        ResultId resultId = new ResultId();
        resultId.setId(testJobRepository.findByDirOfJob(inputDir.getAbsolutePath()).getIdResult());

        return resultId;
    }

    @ApiOperation(value = "return Result Dto (boolean isJobFinished and byte[] result) ")
    @RequestMapping(value = "/check-convert-job/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto getJobResultById(
            @PathVariable(value = "id") Long id
    ) throws Exception {

        ResultDto resultDto;

        Map<String, JobParameter> confMap = new HashMap<String, JobParameter>();
        writeProperties(confMap, Long.toString(System.currentTimeMillis()), "time");
        writeProperties(confMap, id.toString(), "id");

        JobParameters jobParameters = new JobParameters(confMap);
        jobLauncher.run(checkResultJob, jobParameters);


        resultDto = new ResultDto();
        Boolean isFileReady = testJobRepository.getOne(id).getIsJobFinished();
        if (isFileReady == true) {

            resultDto.setJobFinished(isFileReady);
            File resultFile = filesUtil.createResultFile(testJobRepository.getOne(id).getDirOfJob());

            try {
                resultDto.setResult(FileUtils.readFileToByteArray(resultFile));
                FileUtils.deleteDirectory(new File(testJobRepository.getOne(id).getDirOfJob()));
            } catch (IOException e) {
                LOGGER.error("File already deleted:" + testJobRepository.getOne(id).getDirOfJob());
                throw new FileAlreadyDeletedException("File already deleted" + testJobRepository.getOne(id).getDirOfJob());
            }
            return resultDto;
        } else {
            resultDto.setResult(null);
            resultDto.setJobFinished(false);
            return resultDto;
        }

    }


    public void writeProperties(Map<String, JobParameter> propertiesMap, Object property, String propertyName) {
        if (property == null) {
            propertiesMap.put(propertyName, new JobParameter("default"));
            LOGGER.info("Use default value of " + propertyName);
        } else {
            propertiesMap.put(propertyName, new JobParameter("" + property));
        }


    }


}
