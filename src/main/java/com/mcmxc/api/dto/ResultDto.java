package com.mcmxc.api.dto;


import lombok.Data;

@Data
public class ResultDto {

    private boolean isJobFinished;
    private byte[] result;


}
