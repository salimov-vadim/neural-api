package com.mcmxc.api.dto;

import lombok.Data;

@Data
public class ResultId {

    private Long id;
}
