package com.mcmxc.api.repository;

import com.mcmxc.api.entity.ResultOfJob;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestJobRepository extends JpaRepository<ResultOfJob, Long> {

    ResultOfJob findByDirOfJob(String dir);
}
