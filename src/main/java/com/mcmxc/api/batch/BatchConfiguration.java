package com.mcmxc.api.batch;

import com.mcmxc.api.repository.TestJobRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    TestJobRepository testJobRepository;

    @Autowired
    FileTasklet fileTasklet;

    @Bean
    public Step writeInputFilesAndRunShellCommand() {
        return stepBuilderFactory.get("writeInputFiles")
                .tasklet(fileTasklet)
                .build();
    }

    @Bean(value = "pythonJob")
    public Job job(Step writeInputFilesAndRunShellCommand) {
        return jobBuilderFactory.get("pythonJob")
                .incrementer(new RunIdIncrementer())
                .start(writeInputFilesAndRunShellCommand())
                .build();
    }



}
