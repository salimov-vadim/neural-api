package com.mcmxc.api.batch;

import com.mcmxc.api.entity.ResultOfJob;
import com.mcmxc.api.repository.TestJobRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ResultTasklet implements Tasklet {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ResultTasklet.class);

    @Autowired
    private TestJobRepository testJobRepository;

    private byte[] output;


    private ResultOfJob resultOfJob;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)  {
        Long id = Long.valueOf(chunkContext.getStepContext().getJobParameters().get("id").toString());
        resultOfJob = testJobRepository.getOne(id);

        File file = new File(resultOfJob.getDirOfJob() + File.separator + "result.png");


        if (file.exists() && !file.isDirectory()) {
            LOGGER.info(file.getAbsolutePath());
            resultOfJob.setIsJobFinished(true);

            testJobRepository.save(resultOfJob);

            return RepeatStatus.FINISHED;
        } else {
            LOGGER.info("File is not ready");
            return RepeatStatus.FINISHED;
        }


    }
}
