package com.mcmxc.api.batch;

import com.mcmxc.api.contollers.exception.WriteFileException;
import com.mcmxc.api.entity.ResultOfJob;
import com.mcmxc.api.repository.TestJobRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

@Service
public class FileTasklet implements Tasklet {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(FileTasklet.class);

    private File inputDir;

    @Autowired
    private TestJobRepository repo;

    @Value("${value.iteration}")
    String iterations;


    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws WriteFileException {

        StringBuffer shellCommand = new StringBuffer();

        inputDir = new File(chunkContext.getStepContext().getJobParameters().get("inputDir").toString());

        shellCommand.append("start_command ");


        String firstImageName = chunkContext.getStepContext().getJobParameters().get("firstImage").toString();

        if (firstImageName != null) {
            shellCommand.append(" -image command one " + firstImageName);
        } else {
            LOGGER.info("Name of image file can't be null");
            return RepeatStatus.FINISHED;
        }

        String secondImageName = chunkContext.getStepContext().getJobParameters().get("styleImage").toString();

        if (secondImageName != null) {
            shellCommand.append(" -image command two " + secondImageName);
        } else {
            LOGGER.info("Name of image file can't be null");
            return RepeatStatus.FINISHED;
        }


        shellCommand.append(" -result_image " + inputDir.getAbsolutePath() + File.separator + "result.png ");

        if (chunkContext.getStepContext().getJobParameters().get("original").toString() != "default") {
            Boolean original = Boolean.parseBoolean(chunkContext.getStepContext().getJobParameters().get("original").toString());
            if (original == null) {
                LOGGER.info("Use default value of original colors in library");
            } else if (original = true) {
                shellCommand.append("-1 ");
            } else if (original == false) {
                shellCommand.append("-0 ");
            }
        }

        if (chunkContext.getStepContext().getJobParameters().get("weight").toString() != "default") {
            BigDecimal weight = new BigDecimal(chunkContext.getStepContext().getJobParameters().get("weight").toString());
            if (weight != null) {
                shellCommand.append("-weight " + weight.toPlainString() + " ");
            } else {
                LOGGER.info("Use default value of weight in library");
            }
        }

        if (chunkContext.getStepContext().getJobParameters().get("styleWeight").toString() != "default") {
            BigDecimal styleWeight = new BigDecimal(chunkContext.getStepContext().getJobParameters().get("styleWeight").toString());
            if (styleWeight != null) {
                shellCommand.append("-style " + styleWeight.toPlainString() + " ");
            } else {
                LOGGER.info("Use default value of style weight in library");
            }
        }

        if (chunkContext.getStepContext().getJobParameters().get("scale").toString() != "default") {
            Float scale = Float.parseFloat(chunkContext.getStepContext().getJobParameters().get("scale").toString());
            if (scale != null) {
                shellCommand.append("-scale " + scale.toString() + " ");
            } else {
                LOGGER.info("Use default value of style scale in library");
            }
        }

        shellCommand.append(" -test " + iterations);

        ProcessBuilder processBuilder = new ProcessBuilder();


        processBuilder.command("bash", "-c", shellCommand.toString());

        try {
            Process process = processBuilder.start();

        } catch (IOException e) {
            LOGGER.info("Incorrect shell command");
            throw new WriteFileException("exception in shell command");
        }
        LOGGER.info("Shell command: " + shellCommand);


        ResultOfJob resultOfJob = new ResultOfJob();
        resultOfJob.setIsJobFinished(false);
        resultOfJob.setDirOfJob(inputDir.getAbsolutePath());
        repo.save(resultOfJob);

        return RepeatStatus.FINISHED;
    }


}
