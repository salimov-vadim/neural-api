package com.mcmxc.api.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResultBatchConfiguration {

    @Autowired
    private JobBuilderFactory resultJobBuilderFactory;

    @Autowired
    private StepBuilderFactory resultStepBuilderFactory;

    @Autowired
    private ResultTasklet resultTasklet;

    @Bean
    public Step step2() {
        return resultStepBuilderFactory.get("step2")
                .tasklet(resultTasklet)
                .build();
    }

    @Bean(value = "checkIsResultReady")
    public Job job(Step step2)  {
        return resultJobBuilderFactory.get("job2")
                .incrementer(new RunIdIncrementer())
                .start(step2)
                .build();
    }
}
