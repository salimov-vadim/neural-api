package com.mcmxc.api.util;


import com.mcmxc.api.contollers.exception.WriteFileException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
public class FilesUtil {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(FilesUtil.class);


    public static File createTempDirectory(String prefix) {
        final File tmp = new File(FileUtils.
                getTempDirectory().
                getAbsolutePath() + File.separator +
                prefix + (System.currentTimeMillis() * Math.random()));
        tmp.mkdir();
        return tmp;
    }

    public File writeInputFile(MultipartFile file, File inputDir) throws WriteFileException {
        if (file.getOriginalFilename() != null && file.getOriginalFilename().length() > 0) {

            File inputFile = new File(inputDir.getAbsolutePath() + File.separator + file.getOriginalFilename());
            try {
                BufferedOutputStream streamFile = new BufferedOutputStream(new FileOutputStream(inputFile));
                streamFile.write(file.getBytes());
                streamFile.close();
                LOGGER.info("Write input image file: " + inputFile.getAbsolutePath());
                return inputFile;
            } catch (IOException e) {
                LOGGER.info("Write input image file with exception: " + inputFile.getAbsolutePath());
                throw new WriteFileException("Can't write file" + inputFile.getAbsolutePath());
            }

        } else {
            LOGGER.info("Name of file can't be null");
            throw new WriteFileException("Incorrect file name");
        }
    }

    public File createResultFile(String dirOfCurrentJob){
        File resultFile =new File(dirOfCurrentJob + File.separator + "result.png");
        return resultFile;
    }

}
