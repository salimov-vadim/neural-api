package com.mcmxc.api.entity;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class ResultOfJob {

    @Id
    @GeneratedValue
    private Long idResult;

    private Boolean isJobFinished;

    private String dirOfJob;

}
