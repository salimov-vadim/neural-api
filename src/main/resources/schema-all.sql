DROP TABLE result_table IF EXISTS;

CREATE TABLE result_table  (
    result_id IDENTITY NOT NULL PRIMARY KEY,
    is_finished TINYINT,
    result VarBinary
);